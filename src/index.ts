'use strict';
import express = require('express');
import { Messenger,
  Button,
  Element,
  Image,
  Video,
  GenericTemplate,
  ReceiptTemplate,
  Address,
  Summary,
  Adjustment } from 'fbmessenger';
import bodyParser from 'body-parser';

const app: express.Application = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const messenger = new Messenger({
  pageAccessToken: 'EAAFx6Ox242sBAMmoh3OFDNA11PyMpRCLPviODLWYa4wlspuIIBAth0L8q4QI53pRlwZBieM5w1sphIrC75LbzkhPxOgvhsN7poQuIQe8BaN3RIH58226un77tNCopjCqetSJefP6GcZBtbAEgDxZC2hH67Qgjg305FMpqcaIAZDZD'
})

  const WHITELISTED_DOMAINS = [
    'https://bbc.co.uk',
    'https://stackoverflow.com',
  ];

  const timeout = ms => new Promise(resolve => setTimeout(resolve, ms));

  messenger.on('message', async (message) => {
    console.log(`Message received: ${JSON.stringify(message)}`);
    const recipient = message.sender.id;
  
    // Allow receiving locations
    if ('attachments' in message.message) {
      const msgType = message.message.attachments[0].type;
      if (msgType === 'location') {
        console.log('Location received');
        const text = `${message.message.attachments[0].title}:
                      lat: ${message.message.attachments[0].payload.coordinates.lat},
                      long: ${message.message.attachments[0].payload.coordinates.long}`;
        await messenger.send({ text }, recipient);
      }
  
      if (['audio', 'video', 'image', 'file'].includes(msgType)) {
        const attachment = message.message.attachments[0].payload.url;
        console.log(`Attachment received: ${attachment}`);
      }
    }
  
    // Text messages
    if ('text' in message.message) {
      let msg = message.message.text;
      msg = msg.toLowerCase();
  
      if (msg.includes('texto')) {
        await messenger.send({ text: 'Este es un texto de ejemplo.' }, recipient);
      }

      if (msg.includes('hola')) {
        await messenger.send({ text: 'Hola, sea bienvenido a nuestra página, ¿qué desea?.' }, recipient);
      }
      else if (msg.includes('Hola')) {
        await messenger.send({ text: 'Hola, sea bienvenido a nuestra página, ¿qué desea?.' }, recipient);
      }
  
      if (msg.includes('imagen')) {
        await messenger.send({ text: 'Imagen de ejemplo, cargando...' }, recipient);
        const res = await messenger.send(new Image({
          url: 'https://unsplash.it/300/200/?random',
          is_reusable: true,
        }), recipient);
        console.log(`Resuable attachment ID: ${res.attachment_id}`);
      }
  
      if (msg.includes('reuse')) {
        await messenger.send(new Image({ attachment_id: 947782652018100 }), recipient);
      }
  
      if (msg.includes('video')) {
        try {
            await messenger.send({ text: 'Enviando video de ejemplo, espere...' }, recipient);
          await messenger.send(new Video({
            url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4',
          }), recipient);
        } catch (e) {
          console.error(e);
        }
      }
  
      if (msg.includes('payload')) {
        const pl = message.message.quick_reply.payload;
        const text = `Botón clickeado por usuario: ${msg}, El botón payload es: ${pl}`;
        await messenger.send({ text }, recipient);
      }
  
      if (msg.includes('burbuja')) {
        const element = new Element({
          title: 'Ejemplo 1',
          item_url: 'http://www.bbc.co.uk',
          image_url: 'https://unsplash.it/300/200/?random',
          subtitle: 'Abre bbc.co.uk',
          buttons: [
            new Button({
              type: 'web_url',
              title: 'BBC',
              url: 'http://www.bbc.co.uk',
            }),
          ],
        });
        await messenger.send(new GenericTemplate({
          elements: [element],
        }), recipient);
      }
  
      if (msg.includes('multiple')) {
        await messenger.send({ text: 'Mensaje 1' }, recipient);
        await timeout(3000);
        await messenger.send({ text: 'Mensaje 2' }, recipient);
      }
  
      if (msg.includes('recibo')) {
        const template = new ReceiptTemplate({
          recipient_name: 'Nombre',
          order_number: '123',
          currency: 'COP',
          payment_method: 'Visa',
          order_url: 'http://www.ejemplo.com',
          timestamp: '123123123',
          elements: [
            new Element({
              title: 'Título',
              subtitle: 'Subtitulo',
              quantity: 1,
              price: 12.500,
              currency: 'COP',
            }),
          ],
          address: new Address({
            street_1: 'Calle falsa 123',
            street_2: '',
            city: 'Parque Principal',
            postal_code: '00000',
            state: 'BOG',
            country: 'COL',
          }),
          summary: new Summary({
            subtotal: 75000,
            shipping_cost: 4900,
            total_tax: 6100,
            total_cost: 56100,
          }),
          adjustments: [
            new Adjustment({
              name: 'Adjustment',
              amount: 1900,
            }),
          ],
        });
        const res = await messenger.send(template, recipient);
        console.log(res);
      }
    }
  });
  

  messenger.on('postback', (message) => {
    const recipient = message.sender.id;
    const { payload } = message.postback;
    console.log(`Payload received: ${payload}`);
  
    if (payload === 'help') {
      messenger.send({ text: 'A help message or template can go here.' }, recipient);
    } else if (payload === 'START') {
      const text = `Try sending me a message containing one of these keywords:
  text, image, video, reuse, bubble, "quick replies", list, compact, tall, full, nlp, code, or multiple`;
      messenger.send({ text }, recipient);
    }
  });


  app.get('/webhook', (req, res) => {
    let VERIFY_TOKEN = "x"
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];
      
    if (mode && token) {
    
      if (mode === 'subscribe' && token === VERIFY_TOKEN) {
        res.status(200).send(challenge);
      
      } else {
        res.sendStatus(403);      
      }
    }
  });


  app.post('/webhook', (req, res) => {
    res.sendStatus(200);
    messenger.handle(req.body)
  });

app.listen(process.env.PORT || 1337, () => console.log('webhook is listening'));